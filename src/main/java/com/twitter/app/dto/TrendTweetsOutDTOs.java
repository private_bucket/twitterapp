package com.twitter.app.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Collection of {@link TrendTweetsOutDTO} OutDTO
 */
public class TrendTweetsOutDTOs implements OutDTO {

	private static final long serialVersionUID = 2419588348157990896L;
	
	@JsonProperty("Tweets")
	private List<TrendTweetsOutDTO> outDTOs = new ArrayList<>();

	public List<TrendTweetsOutDTO> getOutDTOs() {
		return outDTOs;
	}

	public void setOutDTOs(List<TrendTweetsOutDTO> outDTOs) {
		this.outDTOs = outDTOs;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrendTweetsOutDTOs [outDTOs=");
		builder.append(outDTOs);
		builder.append("]");
		return builder.toString();
	}
}
