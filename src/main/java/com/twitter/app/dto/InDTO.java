package com.twitter.app.dto;

/**
 * Marker dto interface which must be implemented by all other InDTO objects
 */
public interface InDTO {

}
