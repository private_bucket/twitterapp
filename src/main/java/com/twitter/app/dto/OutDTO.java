package com.twitter.app.dto;

import java.io.Serializable;

/**
 * Marker dto interface which must be implemented by all other OutDTO objects
 *
 */
public interface OutDTO extends Serializable {

}
