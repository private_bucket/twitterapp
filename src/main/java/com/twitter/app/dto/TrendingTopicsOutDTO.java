package com.twitter.app.dto;

import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * An OutDTO which contains the relevant outgoing properties of
 * an trending topic
 */
@JsonRootName("Trend")
public class TrendingTopicsOutDTO implements OutDTO {

	private static final long serialVersionUID = -5600538242116413609L;

	private String topic;
	private String url;
	private String query;
	
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrendingTopicsOutDTO [topic=");
		builder.append(topic);
		builder.append(", url=");
		builder.append(url);
		builder.append(", query=");
		builder.append(query);
		builder.append("]");
		return builder.toString();
	}
}
