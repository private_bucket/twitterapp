package com.twitter.app.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * OutDTO which consists of collection of trending topics {@link TrendingTopicsOutDTO}
 */
@JsonRootName(value="Trends")
public class TrendingTopicsOutDTOs implements OutDTO {

	private static final long serialVersionUID = 2831728099190660539L;
	
	@JsonProperty("Trends")
	private List<TrendingTopicsOutDTO> outDTOs = new ArrayList<>();

	public List<TrendingTopicsOutDTO> getOutDTOs() {
		return outDTOs;
	}

	public void setOutDTOs(List<TrendingTopicsOutDTO> outDTOs) {
		this.outDTOs = outDTOs;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrendingTopicsOutDTOs [outDTOs=");
		builder.append(outDTOs);
		builder.append("]");
		return builder.toString();
	}
}
