package com.twitter.app.dto;

/**
 * An OutDTO which contains relevant details of a tweet.
 */
public class TrendTweetsOutDTO implements OutDTO {

	private static final long serialVersionUID = -2755681984210120470L;

	private String trend;
	private String tweet;
	private String userProfilePicture;
	private Integer tweetLikes;
	private Integer tweetRetweets;
	private String userScreenName;
	
	public String getTrend() {
		return trend;
	}

	public void setTrend(String trend) {
		this.trend = trend;
	}
	
	public String getTweet() {
		return tweet;
	}

	public void setTweet(String tweet) {
		this.tweet = tweet;
	}

	public String getUserProfilePicture() {
		return userProfilePicture;
	}

	public void setUserProfilePicture(String userProfilePicture) {
		this.userProfilePicture = userProfilePicture;
	}

	public Integer getTweetLikes() {
		return tweetLikes;
	}

	public void setTweetLikes(Integer tweetLikes) {
		this.tweetLikes = tweetLikes;
	}

	public Integer getTweetRetweets() {
		return tweetRetweets;
	}

	public void setTweetRetweets(Integer tweetRetweets) {
		this.tweetRetweets = tweetRetweets;
	}

	public String getUserScreenName() {
		return userScreenName;
	}

	public void setUserScreenName(String userScreenName) {
		this.userScreenName = userScreenName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrendTweetsOutDTO [trend=");
		builder.append(trend);
		builder.append(", tweet=");
		builder.append(tweet);
		builder.append(", userProfilePicture=");
		builder.append(userProfilePicture);
		builder.append(", tweetLikes=");
		builder.append(tweetLikes);
		builder.append(", tweetRetweets=");
		builder.append(tweetRetweets);
		builder.append(", userScreenName=");
		builder.append(userScreenName);
		builder.append("]");
		return builder.toString();
	}
}
