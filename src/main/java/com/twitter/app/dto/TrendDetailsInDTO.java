/**
 * 
 */
package com.twitter.app.dto;

/**
 * An InDTO which contains basic details of a trend
 */
public class TrendDetailsInDTO implements InDTO {

	private String trend;
	private String query;
	private String url;
	
	public String getTrend() {
		return trend;
	}
	public void setTrend(String trend) {
		this.trend = trend;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrendDetailsInDTO [trend=");
		builder.append(trend);
		builder.append(", query=");
		builder.append(query);
		builder.append(", url=");
		builder.append(url);
		builder.append("]");
		return builder.toString();
	}
}
