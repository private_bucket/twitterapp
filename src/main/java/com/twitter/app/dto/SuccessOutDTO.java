package com.twitter.app.dto;

/**
 * OutDTO to be used to indicate the success of operation
 */
public class SuccessOutDTO implements OutDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 519387136357497640L;
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SuccessOutDTO [id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}
}
