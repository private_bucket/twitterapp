package com.twitter.app.command;

import com.twitter.app.core.Constants;
import com.twitter.app.dto.InDTO;
import com.twitter.app.dto.OutDTO;
import com.twitter.app.exception.RestAPIException;

/**
 * POST command which must implement steps of command execution.
 * <br>
 * Default behavior is intentional to provide step execution (concrete) at runtime.
 */
public class PostDtoCommand extends DtoCommand {

	@Override
	public InDTO getDTO() throws RestAPIException {
		throw new RestAPIException("getDTO() must be implemented", Constants.MISSING_IMPLEMETATION);
	}

	@Override
	public OutDTO execute(InDTO inDTO) throws RestAPIException {
		throw new RestAPIException("execute() must be implemented", Constants.MISSING_IMPLEMETATION);
	}
}
