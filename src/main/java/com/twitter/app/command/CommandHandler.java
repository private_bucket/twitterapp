package com.twitter.app.command;

import org.springframework.http.HttpStatus;

import com.twitter.app.core.ErrorData;
import com.twitter.app.core.ResponseData;
import com.twitter.app.dto.OutDTO;
import com.twitter.app.exception.RestAPIException;

/**
 * Abstract handler which setups commands generic response
 */
public abstract class CommandHandler implements Handler{

	protected ResponseData responseData;
	protected DtoCommand command;
	
	{
		responseData = new ResponseData();
	}
	
	public CommandHandler(DtoCommand command)
	{
		this.command = command;
	}
	
	/**
	 * Method which processes successful response.
	 * 
	 * @param outDTO corresponding to requested API successful response.
	 * @return ResponseData generic response to API services
	 */
	protected ResponseData processResponse(OutDTO outDTO)
	{
		responseData.setData(outDTO);
		responseData.setStatus(HttpStatus.OK.value());
		return responseData;
	}
	
	/**
	 * Method which process the error responses.
	 * @param exception which contains the error code which specifies the cause of error in the system.
	 * @return ResponseData generic response to API services
	 */
	protected ResponseData handlerError(RestAPIException exception)
	{
		//NOTE : due to short time to implement streaming app,
		//improvement to get error message from localized bundle would have been done
		if( exception.getErrorData().size() > 0)
		{
			responseData.setError(exception.getErrorData());
		}
		else
		{
			responseData.getError().add(new ErrorData(exception.getKey(), exception.getMessage() ) );
		}
		
		responseData.setStatus(exception.getErrorTypeCode().value());
		
		return responseData;
	}
}
