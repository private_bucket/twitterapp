package com.twitter.app.command;

import java.util.ArrayList;
import java.util.List;

import com.twitter.app.core.ErrorData;
import com.twitter.app.dto.InDTO;
import com.twitter.app.exception.RestAPIException;


/**
 * Abstract DTO command which mandates to actual command to extend
 * 
 * Each command i.e. GET or POST etc can have it's defined nature of execution {@link GetDtoCommand}
 * <br>
 * e.g GET command does not have a HTTP body hence it generally does not include of any {@link InDTO}
 * <br>
 */
public abstract class DtoCommand implements Command {

	/**
	 * Method which is used to validate the incoming request.
	 * Supported only for PUT and POST commands.
	 */
	public List<ErrorData> validate(InDTO inDTO) throws RestAPIException
	{
		List<ErrorData> errorData = new ArrayList<>();

		//TODO : Due to lack of time this is not implemented.
		
		//Design decisions and considerations could be
		//1. Add support for BeanValidator
		//2. get the bean validator object
		//3. validate the InDTO
		//4. If any constrain violations returned , create list of ErrorData and take the localized error validation messages
		//     from resource bundle
		
		return errorData;
	}
}
