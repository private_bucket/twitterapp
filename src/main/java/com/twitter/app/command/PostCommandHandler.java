package com.twitter.app.command;

import com.twitter.app.core.ResponseData;
import com.twitter.app.dto.InDTO;
import com.twitter.app.dto.OutDTO;
import com.twitter.app.exception.RestAPIException;

/**
 * Concrete GET command handler which provides <i><u>handling</u></i> of actual command request.
 */
public class PostCommandHandler extends CommandHandler {

	public PostCommandHandler(DtoCommand command) {
		super(command);
	}

	@Override
	public ResponseData handle() {
		OutDTO outDTO = null;
		try {
			// prepare the InDTO required for this requested API
			InDTO inDTO = command.getDTO();
			// Validate the request 
			command.validate(inDTO);
			//execute the command
			outDTO = command.execute(inDTO);
			//process the response
			responseData = this.processResponse(outDTO);
		} catch (RestAPIException e) {
			//controllers MUST throw RestAPIException always which is designed to be a wrapped exception
			responseData = this.handlerError(e);
		}
		
		return responseData;
	}

}
