package com.twitter.app.command;

import com.twitter.app.core.ResponseData;

/**
 * Generic interface for processing actual command requested
 */
public interface Handler {

	/**
	 * Method which handles the steps of command executions for respective command handler
	 * 
	 * @return ResponseData could be a successful response with API response or it could be the error
	 */
	public ResponseData handle();
}
