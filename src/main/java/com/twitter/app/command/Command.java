package com.twitter.app.command;

import java.util.List;

import com.twitter.app.core.ErrorData;
import com.twitter.app.dto.InDTO;
import com.twitter.app.dto.OutDTO;
import com.twitter.app.exception.RestAPIException;

/**
 * Command interface which must be implemented to execute 
 * commands like GET, POST, PUT etc
 */
public interface Command {
	
	/**
	 * Method which returns the InDTO involved in the request
	 * @return API specific InDTO
	 * @throws RestAPIException
	 */
	public InDTO getDTO() throws RestAPIException;
	
	/**
	 * Method which define the execution of command
	 * @param inDTO request InDTO which contains request data for execution
	 * @return OutDTO respective OutDTO for the API
	 * @throws RestAPIException
	 */
	public OutDTO execute(InDTO inDTO) throws RestAPIException;
	
	/**
	 * Method which validates the incoming request before proceeding with actual business execution
	 * @param inDTO request InDTO to be validated
	 * @return collection of validation errors if any
	 * @throws RestAPIException
	 */
	public List<ErrorData> validate(InDTO inDTO) throws RestAPIException;
}
