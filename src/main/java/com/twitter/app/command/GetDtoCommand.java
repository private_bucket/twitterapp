package com.twitter.app.command;

import java.util.List;

import com.twitter.app.core.Constants;
import com.twitter.app.core.ErrorData;
import com.twitter.app.dto.InDTO;
import com.twitter.app.dto.OutDTO;
import com.twitter.app.exception.RestAPIException;

/**
 * GET command which must implement steps of command execution.
 * <br>
 * Default behavior is intentional to provide step execution (concrete) at runtime.
 * <br>
 * GET command does not have HTTP body generally hence it does not support {@link InDTO}
 */
public class GetDtoCommand extends DtoCommand {
	
	@Override
	public InDTO getDTO() throws RestAPIException {
		throw new UnsupportedOperationException("Operation not supported for this command");
	}

	@Override
	public OutDTO execute(InDTO inDTO) throws RestAPIException {
		throw new RestAPIException("execute() must be implemented", Constants.MISSING_IMPLEMETATION);
	}

	@Override
	public List<ErrorData> validate(InDTO inDTO) throws RestAPIException {
		throw new UnsupportedOperationException("Operation not supported for this command");
	}
}
