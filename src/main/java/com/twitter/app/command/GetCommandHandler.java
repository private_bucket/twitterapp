package com.twitter.app.command;

import com.twitter.app.core.ResponseData;
import com.twitter.app.dto.OutDTO;
import com.twitter.app.exception.RestAPIException;

/**
 * Concrete GET command handler which provides <i><u>handling</u></i> of actual command request.
 */
public class GetCommandHandler extends CommandHandler {

	public GetCommandHandler(DtoCommand command) {
		super(command);
	}

	@Override
	public ResponseData handle() {
		
		//execute the command
		OutDTO outDTO = null;
		try {
			outDTO = command.execute(null);
			//process the response
			responseData = this.processResponse(outDTO);
		} catch (RestAPIException e) {
			//controllers MUST throw RestAPIException always which is designed to be a wrapped exception
			responseData = this.handlerError(e);
		}
		
		//return response to client
		return responseData;
	}
}
