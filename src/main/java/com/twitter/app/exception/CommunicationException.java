package com.twitter.app.exception;

/**
 * Exception that must be thrown when any error during external system communication
 * is occurred.
 */
public class CommunicationException extends RestAPIException {

	private static final long serialVersionUID = -4495762080112333699L;
	
	public CommunicationException(String message, String key) {
		super(message, key);
	}

}
