package com.twitter.app.exception;

import java.util.ArrayList;
import java.util.List;

import com.twitter.app.core.ErrorData;
import com.twitter.app.core.ErrorTypeCode;
 
/**
 * Wrapper exception which must wrap all other exceptions thrown from the
 * system. This must be the generic exception that needs to be processed
 * to create final graceful error response back to client
 */
public class RestAPIException extends Exception {

	private static final long serialVersionUID = -2005461059437119136L;
	private String message;
	private String key;
	private List<ErrorData> errorData = new ArrayList<>();
	private ErrorTypeCode errorTypeCode;
	
	public RestAPIException(String message, String key)
	{
		this.message = message;
		this.key = key;
	}
	
	public RestAPIException(List<ErrorData> errorData)
	{
		this.errorData = errorData;
	}

	public String getMessage() {
		return message;
	}

	public String getKey() {
		return key;
	}

	public List<ErrorData> getErrorData() {
		return errorData;
	}
	
	public ErrorTypeCode getErrorTypeCode() {
		
		if( errorTypeCode != null)
			return errorTypeCode;
		
		return ErrorTypeCode.INTERNAL_SERVER;
	}

	public void setErrorTypeCode(ErrorTypeCode errorTypeCode) {
		this.errorTypeCode = errorTypeCode;
	}
}
