package com.twitter.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.twitter.app.command.CommandHandler;
import com.twitter.app.command.PostCommandHandler;
import com.twitter.app.command.PostDtoCommand;
import com.twitter.app.dto.InDTO;
import com.twitter.app.dto.OutDTO;
import com.twitter.app.dto.SuccessOutDTO;
import com.twitter.app.dto.TrendDetailsInDTO;
import com.twitter.app.exception.RestAPIException;
import com.twitter.app.service.TwitterService;

/**
 * Controller which enables streaming messaging services along with any REST services.
 */
@Controller
public class StreamController {

    @Autowired
	TwitterService twitterService;

    @RequestMapping(value = "/")
    public String home(){
        return "home";
    }

    @MessageMapping("/handshek")
    public void handshek(final TrendDetailsInDTO trendDetailsInDTO) throws RestAPIException {
        
		CommandHandler command = new PostCommandHandler( new PostDtoCommand() {
			
			@Override
			public InDTO getDTO() {
				return trendDetailsInDTO;
			}
			
			@Override
			public OutDTO execute(InDTO inDTO) throws RestAPIException {
				
				TrendDetailsInDTO trendDetailsInDTO = (TrendDetailsInDTO)inDTO;
				twitterService.initiateTwitterStream( trendDetailsInDTO.getTrend() );
				
				return new SuccessOutDTO();
				
			}
		});
		
		//process
		command.handle();
    }
}
