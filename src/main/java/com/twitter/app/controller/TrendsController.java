/**
 * 
 */
package com.twitter.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import twitter4j.Trend;
import twitter4j.Trends;

import com.twitter.app.command.CommandHandler;
import com.twitter.app.command.GetCommandHandler;
import com.twitter.app.command.GetDtoCommand;
import com.twitter.app.core.ResponseData;
import com.twitter.app.dto.InDTO;
import com.twitter.app.dto.OutDTO;
import com.twitter.app.dto.TrendingTopicsOutDTO;
import com.twitter.app.dto.TrendingTopicsOutDTOs;
import com.twitter.app.exception.RestAPIException;
import com.twitter.app.service.TwitterService;

/**
 * Controller which exposes API's related to twitter trends
 */
@Controller
@RequestMapping(value = "/app")
public class TrendsController {

	@Autowired
	TwitterService twitterService;
	
	@RequestMapping(value = "/poll/trends/{locationId}", method = RequestMethod.GET)
	public
	@ResponseBody ResponseData trends(@PathVariable("locationId") final Integer woeid) throws RestAPIException
	{
		CommandHandler command = new GetCommandHandler(new GetDtoCommand() {
			@Override
			public OutDTO execute(InDTO inDTO) throws RestAPIException {
				
				Trends trends = null;
				TrendingTopicsOutDTOs outDTOs = new TrendingTopicsOutDTOs();
				try
				{
					trends = twitterService.getTrendsByPlace( woeid );
					
					for(Trend trend : trends.getTrends())
					{
						TrendingTopicsOutDTO outDTO = new TrendingTopicsOutDTO();
						outDTO.setQuery(trend.getQuery());
						outDTO.setTopic(trend.getName());
						outDTO.setUrl(trend.getURL());
						
						outDTOs.getOutDTOs().add(outDTO);
					}
				}
				catch(RestAPIException e)
				{
					throw e;
				}
				
				return outDTOs;
			}
		});
		
		ResponseData responseData = command.handle();
		
		return responseData;
	}
}
