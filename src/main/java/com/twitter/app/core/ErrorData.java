package com.twitter.app.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Error data which contains,
 * <ul>
 *   <li> Error Code </li>
 *   <li> Error Key </li> 
 *   <li> Error Message </li>
 * </ul>
 */
@JsonInclude(Include.NON_NULL)
public class ErrorData {

	private Integer errorCode;
	private String errorKey;
	private String errorMessage;
	
	public ErrorData(Integer errorCode, String errorKey, String errorMessage)
	{
		this.errorCode = errorCode;
		this.errorKey = errorKey;
		this.errorMessage = errorMessage;
	}
	
	public ErrorData(String errorKey, String errorMessage)
	{
		this(null, errorKey, errorMessage);
	}
	
	public ErrorData(Integer errorCode, String errorMessage)
	{
		this(errorCode, null, errorMessage);
	}
	
	public Integer getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorKey() {
		return errorKey;
	}
	public void setErrorKey(String errorKey) {
		this.errorKey = errorKey;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ErrorData [errorCode=");
		builder.append(errorCode);
		builder.append(", errorKey=");
		builder.append(errorKey);
		builder.append(", errorMessage=");
		builder.append(errorMessage);
		builder.append("]");
		return builder.toString();
	}
}
