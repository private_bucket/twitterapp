/**
 * 
 */
package com.twitter.app.core;

/**
 * Application Constants
 */
public final class Constants {

	/*************** General constants ****************************/
	public final static String FAILED_GET_TRENDS = "FAILED_GET_TRENDS";
	public final static String FAILED_GET_TRENDS_UPDATE = "FAILED_GET_TRENDS_UPDATE";
	public final static String FAILED_TREND_STREAMING = "FAILED_TREND_STREAMING";
	public final static String MISSING_IMPLEMETATION = "MISSING_IMPLEMETATION";
}
