/**
 * 
 */
package com.twitter.app.core;

/**
 * Error can be of multiple types based on layer which it occurs
 */
public enum ErrorTypeCode {

	VALIDATION(201),
	BUSINESS(202),
	INTERNAL_SERVER(500);
	
	private Integer errorTypeCode;
	
	private ErrorTypeCode(Integer errorTypeCode)
	{
		this.errorTypeCode = errorTypeCode;
	}
	
	public Integer value()
	{
		return this.errorTypeCode;
	}
}
