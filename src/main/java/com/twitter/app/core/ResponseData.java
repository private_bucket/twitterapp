package com.twitter.app.core;

import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Communication protocol from server to client
 */
@JsonInclude(Include.NON_NULL)
public class ResponseData {

	private Integer status;
	private List<ErrorData> error;
	private Object data;
	private List<Object> list;
	private String message;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public List<ErrorData> getError() {
		return error;
	}
	public void setError(List<ErrorData> error) {
		this.error = error;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public Collection<Object> getList() {
		return list;
	}
	public void setList(List<Object> list) {
		this.list = list;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResponseData [status=");
		builder.append(status);
		builder.append(", error=");
		builder.append(error);
		builder.append(", data=");
		builder.append(data);
		builder.append(", list=");
		builder.append(list);
		builder.append(", message=");
		builder.append(message);
		builder.append("]");
		return builder.toString();
	}
}
