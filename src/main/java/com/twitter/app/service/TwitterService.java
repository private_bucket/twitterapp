package com.twitter.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.Trends;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.User;

import com.twitter.app.core.Constants;
import com.twitter.app.core.ResponseData;
import com.twitter.app.dto.TrendTweetsOutDTO;
import com.twitter.app.exception.RestAPIException;

/**
 * Service which provides services related to twitter services
 */
@Service
public class TwitterService {

	@Autowired
	private SimpMessagingTemplate template;


	public Trends getTrendsByPlace(Integer woeid) throws RestAPIException {

		Trends trends = null;
		try
		{
			Twitter twitter = TwitterFactory.getSingleton();
			trends = twitter.getPlaceTrends( woeid );
		}
		catch(TwitterException e)
		{
			throw new RestAPIException("Failed to get trending topics", Constants.FAILED_GET_TRENDS);
		}

		return trends;
	}
	
	public void initiateTwitterStream(final String trend) throws RestAPIException {
		
		TwitterStream twitterStream = null;
		
		twitterStream = TwitterStreamFactory.getSingleton();

		StatusListener statusListener = new StatusListener() {

			@Override
			public void onException(Exception ex) {
				// not used / required at this stage
			}

			@Override
			public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
				// not used / required at this stage
			}

			@Override
			public void onStatus(Status status) {
				
				// a new update has been encountered on this trend / topic
				TrendTweetsOutDTO update = new TrendTweetsOutDTO();
				User user = status.getUser();
				update.setUserScreenName(user.getScreenName());
				String imageURL = null;
				if( user.getBiggerProfileImageURL() != null)
				{
					imageURL = user.getBiggerProfileImageURL();
				}
				else
				if ( user.getMiniProfileImageURL() != null)
				{
					imageURL = user.getMiniProfileImageURL();
				}
				else
				{
					imageURL = user.getProfileImageURL();
				}
				
				update.setUserProfilePicture(imageURL);
				update.setTweetLikes(status.getFavoriteCount());
				update.setTweetRetweets(status.getRetweetCount());
				update.setTrend( trend );
				update.setTweet(status.getText());
				
				ResponseData responseData = new ResponseData();
				responseData.setData(update);
				
				// Message broker publishes the messages once it receives from streaming
				// these messages are then converted (JSON) and sent over the client stream
				// over registered STOMP protocol through websocket.
				template.convertAndSend("/topic/updates", responseData);
			}

			@Override
			public void onStallWarning(StallWarning warning) {
				// not used / required at this stage
			}

			@Override
			public void onScrubGeo(long userId, long upToStatusId) {
				// not used / required at this stage
			}

			@Override
			public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
				// not used / required at this stage as we are not storing 
				// any trend or tweet so nothing to delete
			}
		};

		//register the status listened so that whenever a new tweet is posted
		//on this trend then it streams to client 
		twitterStream.addListener(statusListener);
		//define the trend as in track to be searched and whose tweets then streamed to client
		FilterQuery filterQuery = new FilterQuery( trend );
		//initialize the realtime streaming and thread over internally 
		twitterStream.filter(filterQuery);
	}
}
