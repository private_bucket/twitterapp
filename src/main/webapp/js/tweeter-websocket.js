/**
 * 
 */
var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected); 
    $("#trendify").prop("disabled", !connected); 
}

function connect() {
	var socket = new SockJS('/twitter-app/handshek');
	stompClient = Stomp.over(socket);
	stompClient.connect({}, function(frame) {
		setConnected(true);
		console.log('Connected: ' + frame);
		stompClient.subscribe('/topic/updates', function(update){
			renderUpdate(update);
		});
	});
}

function disconnect()
{
	if (stompClient != null) {
		stompClient.disconnect();
	}

	setConnected(false);
	$("#trends").html("");
	
	console.log("Disconnected");
}

function renderUpdate(message) 
{
	// TODO : swingout_loader('modal-loader');
	
	var details = JSON.parse(message.body);
	
	$("div#tweetSpace").removeClass('hide');
    $("#tbody_tweets").prepend(
    		"<tr><td>"+details.data.tweet+"</td></tr>");
}

$(document).ready(function(){

    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#trendify").click(function() {

    	swingin_loader('loader');
    	
    	var woeid = $("select#trendingCountry").val();
    	
    	$.get('app/poll/trends/'+woeid, function(data, status){
    		$.each(data.data.Trends, function(index, value){
    			$("#conversation").show	();
    		    $("#tbody_trends").append(
    		    		"<tr><td>" + value.topic + "</td>"
    		    		+ "<td><a id='"+index+"' style='cursor: pointer;' data-toggle='modal' data-target='#trendsModal' data-trend='"+ value.topic +"' title='View'><span class='glyphicon glyphicon-eye-open'></span></a></td></tr>");
    		});
    		
    		swingout_loader('loader');
    	});
    	
    	
    });
    
    $('#tbody_trends').on('click', 'a', function(){
    	
    	//get the trend name
    	var trend = $(this).data('trend');
    	//TODO : swingin_loader('modal-loader');
    	stompClient.send("/app/handshek", {}, JSON.stringify({ 'trend': trend }));
    });
    
    //clear the existing modal content when current trends modal is closed/hide
    $(".modal").on("hidden.bs.modal", function(){
        $(".modal-body #tbody_tweets").html("");
    });
    
    $("#conversation").hide();
});